package com.dto;

import java.sql.Timestamp;
public class UserDto {
    private Integer id;
    private String groupname;
    private String username;
    private String password;
    private Integer groupid;
    private Timestamp creattime;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getGroupname() {
        return groupname;
    }
    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Integer getGroupid() {
        return groupid;
    }
    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }
    public Timestamp getCreattime() {
        return creattime;
    }
    public void setCreattime(Timestamp creattime) {
        this.creattime = creattime;
    }
    
    
}
