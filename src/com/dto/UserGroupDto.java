package com.dto;

public class UserGroupDto {
    private Integer id;
    private String groupname;
    private String groupdes;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getGroupname() {
        return groupname;
    }
    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
    public String getGroupdes() {
        return groupdes;
    }
    public void setGroupdes(String groupdes) {
        this.groupdes = groupdes;
    }
    
}
