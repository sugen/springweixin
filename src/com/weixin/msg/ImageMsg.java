package com.weixin.msg;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
public class ImageMsg extends Msg {
	private List<String> mediaIds;
	public ImageMsg() {
		// TODO Auto-generated constructor stub
		this.msgType=MsgEmum.image.toString();
		mediaIds=new ArrayList<String>();
	}
	@XmlElementWrapper(name="Image")
	@XmlElement(name="MediaId")
	public List<String> getMediaIds() {
		return mediaIds;
	}
	public void setMediaIds(List<String> mediaIds) {
		this.mediaIds = mediaIds;
	}
	public void AddMediaId(String mediaId){
		mediaIds.add(mediaId);
	}
}
