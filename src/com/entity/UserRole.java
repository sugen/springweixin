package com.entity;

/**
 * UserRole entity. @author MyEclipse Persistence Tools
 */

public class UserRole implements java.io.Serializable {

    // Fields

    private UserRoleId id;
    private User user;

    // Constructors

    /** default constructor */
    public UserRole() {
    }

    /** minimal constructor */
    public UserRole(UserRoleId id) {
	this.id = id;
    }

    /** full constructor */
    public UserRole(UserRoleId id, User user) {
	this.id = id;
	this.user = user;
    }

    // Property accessors

    public UserRoleId getId() {
	return this.id;
    }

    public void setId(UserRoleId id) {
	this.id = id;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

}