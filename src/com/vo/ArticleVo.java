package com.vo;
/**
 * 
 * ArticleVo.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午6:44:10 
 * @version
 * @user micrxdd
 */
public class ArticleVo {
    private String title;
    private Integer id;
    private String html;
    private Integer typeid;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getHtml() {
        return html;
    }
    public void setHtml(String html) {
        this.html = html;
    }
    public Integer getTypeid() {
        return typeid;
    }
    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
