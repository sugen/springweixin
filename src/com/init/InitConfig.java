package com.init;

import javax.annotation.Resource;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Repository;

import com.service.ConfigService;
import com.service.MsgService;

/**
 * InitConfig.java
 * 
 * @author microxdd
 * @version 创建时间：2014 2014年9月20日 下午2:02:43 micrxdd
 * 
 */
@Repository
public class InitConfig implements ApplicationListener<ContextRefreshedEvent>{
    @Resource
    private ConfigService configService;
    @Resource
    private MsgService msgService;
    
    //避免多次
    private Boolean b = false;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
	System.out.println("事件");
	System.out.println(arg0);
	// TODO Auto-generated method stub
	if (!b) {
	    System.out.println("spring 启动完成,接下来获取系统配置和初始化消息规则");
	    configService.initConfig();
	    msgService.MsgRolesInit();
	    b=true;
	}

    }

}
