package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.TextDao;
import com.entity.Text;
@Repository("textDao")
public class TextDaoImpl extends BaseDaoImpl<Text> implements TextDao{

}
