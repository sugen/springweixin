package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.NewsDao;
import com.entity.News;
@Repository("newsDao")
public class NewsDaoImpl extends BaseDaoImpl<News> implements NewsDao{
}
