package com.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;

import com.dao.BaseDao2;

public class BaseDao2Impl implements BaseDao2{
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	@Override
	public <T> void save(T entity) {
		// TODO Auto-generated method stub
		getSession().save(entity);
	}

	@Override
	public <T> void del(T entity) {
		// TODO Auto-generated method stub
		getSession().delete(entity);
	}

	@Override
	public <T> void update(T entity) {
		// TODO Auto-generated method stub
		getSession().update(entity);
	}

	@Override
	public <T> T findById(Class<T> clazz,int id) {
		// TODO Auto-generated method stub
		return (T) getSession().get(clazz, id);
	}

	@Override
	public <T> List<T> findAll(Class<T> clazz,Criterion... pamas) {
		// TODO Auto-generated method stub
		Criteria criteria= getSession().createCriteria(clazz);
		if(pamas!=null){
			for (Criterion criterion : pamas) {
				criteria.add(criterion);
			}
		}
		return criteria.list();
	}

}
