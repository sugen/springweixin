package com.service;

import java.util.List;

import com.dto.NewsTypeDto;
import com.dto.Pageinfo;
import com.entity.Newstype;
import com.vo.NewsTypeVo;
/**
 * 新闻分类服务
 * NewsTypeService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午4:03:07 
 * @version
 * @user micrxdd
 */
public interface NewsTypeService {
    /**
     * 获取根分类
     * @return
     */
    public List<Newstype> NewsTypeRootList(Pageinfo pageinfo);
    /**
     * 返回新闻分类服务
     * @return
     */
    public List<NewsTypeDto> NewsTypeList();
    /**
     * 保存一个新闻分类
     * @param newsTypeVo
     */
    public void SaveNewsType(NewsTypeVo newsTypeVo);
    /**
     * 更新一个新闻分类
     * @param newsTypeVo
     */
    public void UpdateNewsType(NewsTypeVo newsTypeVo);
    /**
     * 删除一个新闻分类
     * @param id
     */
    public void DelNewsType(Integer id);
    /**
     * 根据id获取这个新闻分类
     * @param id
     * @return
     */
    public NewsTypeVo NewsTypeById(Integer id);
}
