package com.service;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;

public interface CopyOfMsgPlueService {
	ApplicationContext applicationContext=null;
	public void setApplicationContext(ApplicationContext applicationContext);
	public void addBeanService(Service service);
	Class<?> getServiceClass(String className);
	public void registerBean(String beanName, BeanDefinition beanDefinition);
}
