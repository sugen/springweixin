package com.service.impl;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.ResourceEntityResolver;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;

import com.service.PluginServer;
import com.util.ApplicationContextUtil;

public class PluginServerImpl implements PluginServer {
	@Resource
	private ApplicationContextUtil applicationContextUtil;

	public void loadBean(String configLocationString) {
		ApplicationContext applicationContext = applicationContextUtil
				.getApplicationContext();
		XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(
				(BeanDefinitionRegistry) applicationContext
						.getParentBeanFactory());

		beanDefinitionReader.setResourceLoader(applicationContextUtil
				.getApplicationContext());
		beanDefinitionReader.setEntityResolver(new ResourceEntityResolver(
				applicationContext));
		try {
			String[] configLocations = new String[] { configLocationString };
			for (int i = 0; i < configLocations.length; i++)
				beanDefinitionReader.loadBeanDefinitions(applicationContext
						.getResources(configLocations[i]));
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
